use std::env::var;

fn main() {
    // As recommended by https://doc.rust-lang.org/cargo/reference/build-scripts.html#cargorerun-if-changedpath because we don't need to rerun because of external causes.
    println!("cargo:rerun-if-changed=build.rs");

    // Forward cargo environment variables to the library.

    println!(
        "cargo:rustc-env=TREADMILL_TARGET={}",
        var("TARGET").unwrap()
    );

    println!("cargo:rustc-env=TREADMILL_HOST={}", var("HOST").unwrap());

    println!(
        "cargo:rustc-env=TREADMILL_ENCODED_FLAGS={}",
        var("CARGO_ENCODED_RUSTFLAGS").unwrap()
    );

    println!(
        "cargo:rustc-env=TREADMILL_OUT_DIR={}",
        var("OUT_DIR").unwrap()
    );
}
