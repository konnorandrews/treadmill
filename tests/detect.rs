use cargo_manifest::Edition;
use treadmill::environment::Environment;

#[test]
fn can_detect_x86_64_linux_gnu() {
    let detected = Environment::detect().unwrap();

    #[cfg(all(
        target_arch = "x86_64",
        target_vendor = "unknown",
        target_os = "linux",
        target_env = "gnu"
    ))]
    assert_eq!(detected.toolchain.target_triple, "x86_64-unknown-linux-gnu");

    assert_eq!(detected.package.name, "treadmill");
    assert!(detected.package.manifest_dir.join("Cargo.toml").exists());
    assert_eq!(detected.test.name, "detect");
    assert_eq!(detected.package.edition, Edition::E2021);
    assert!(detected.package.target_directory.exists());
}
