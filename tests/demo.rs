use treadmill::program;

#[test]
fn test2() {
    let binary = program!(
        {
            #![treadmill(disable_inline_check)]
            use toml;

            pub fn main() {
                // println!("hello world {}", rand::random::<char>());

                // dbg!(std::env::vars_os().collect::<Vec<_>>());
                // todo!();

                treadmill::demo();
                // unsafe { std::hint::unreachable_unchecked() };
            }
        }
    )
    // .dependency(
    //     "rand",
    //     DependencyDetail {
    //         version: Some("*".into()),
    //         ..Default::default()
    //     },
    // )
    .compile();

    let out = binary.command().output().unwrap();
    println!("{}", String::from_utf8(out.stdout).unwrap());
    println!("{}", String::from_utf8(out.stderr).unwrap());
    // todo!()
}

// #[test]
// fn test1() {
//     let compile_error = program!(
//         {
//             #![treadmill(disable_inline_check)]
//
//             pub fn main() {
//                 123
//             }
//         }
//     )
//     .compile_should_fail();
//
//     insta::assert_display_snapshot!(
//         compile_error,
//         @r###"
//     error[E0308]: mismatched types
//      --> main.rs:2:5
//       |
//     1 | pub fn main() {
//       |              - expected `()` because of default return type
//     2 |     123
//       |     ^^^ expected `()`, found integer
//     "###
//     );
// }

// /// Test the inline check of the program! macro by using treadmill twice.
// #[test]
// #[cfg(not(coverage))]
// fn nested() {
//     let compile_error = program!(
//         {
//             #![treadmill(disable_inline_check)]
//
//             use treadmill::program;
//
//             pub fn main() {
//                 program!({
//                     pub fn main() {
//                         123
//                     }
//                 });
//             }
//         }
//     )
//     .compile_should_fail();
//
//     insta::assert_snapshot!(
//         compile_error,
//         @r###"
//     error[E0308]: mismatched types
//      --> main.rs:3:32
//       |
//     3 |     program!({ pub fn main() { 123 } });
//       |                             -  ^^^ expected `()`, found integer
//       |                             |
//       |                             expected `()` because of default return type
//     "###
//     );
// }

// #[test]
// fn with_miri() {
//     let binary = program!(
//         {
//             #![treadmill(disable_inline_check)]
//
//             use treadmill::program;
//
//             pub fn main() {
//                 program!({
//                     #![treadmill(disable_inline_check)]
//
//                     pub fn main() {
//                         123
//                     }
//                 });
//             }
//         }
//     )
//     .compile();
//
//     let out = binary.command().output().unwrap();
//     println!("{}", String::from_utf8(out.stdout).unwrap());
//     println!("{}", String::from_utf8(out.stderr).unwrap());
//     todo!()
// }
