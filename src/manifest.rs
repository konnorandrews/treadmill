use cargo_manifest::{
    Manifest, MaybeInherited, Package, Product, Publish, StringOrBool,
    Workspace,
};

use crate::Program;

/// Create the full Cargo manifest for a program.
pub fn make_manifest(program: &Program) -> Manifest {
    Manifest {
        package: Some(Package {
            name: program.name.clone(),
            edition: Some(MaybeInherited::Local(
                // Use same edition as parent package.
                program.environment.package.edition,
            )),
            version: None, // Doesn't need a version.
            build: Some(StringOrBool::Bool(false)), // No build script.
            workspace: None, // Not in a workspace.
            // workspace: Some(program.environment.package.manifest_dir.to_string()),
            authors: None,             // Not needed.
            links: None,               // Not needed.
            description: None,         // Not needed.
            homepage: None,            // Not needed.
            documentation: None,       // Not needed.
            readme: None,              // Not needed.
            keywords: None,            // Not needed.
            categories: None,          // Not needed.
            license: None,             // Not needed.
            license_file: None,        // Not needed.
            repository: None,          // Not needed.
            metadata: None,            // Not needed.
            rust_version: None,        // Not needed.
            exclude: None,             // Not needed.
            include: None,             // Not needed.
            default_run: None,         // Not needed.
            autobins: Some(false),     // Don't need to search for bins.
            autoexamples: Some(false), // Don't need to search for examples.
            autotests: Some(false),    // Don't need to search for tests.
            autobenches: Some(false),  // Don't need to search for benches.
            // Don't allow publishing this crate.
            publish: Some(MaybeInherited::Local(Publish::Flag(false))),
            resolver: None, // Set on the workspace.
        }),
        // Use the cargo features set for the program.
        cargo_features: program.manifest.cargo_features.clone(),
        // Force the package to be in its own workspace to make cargo happy
        // with parent packages living in a workspace.
        // workspace: None,
        workspace: Some(Workspace {
            // Inherit the resolver setting from the parent package.
            resolver: Some(program.environment.package.resolver),
            ..Workspace::default()
        }),
        // Use the dependencies set for the program.
        // dependencies: Some(program.manifest.dependencies.clone()),
        dependencies: None,
        dev_dependencies: None, // No dev dependencies are needed.
        build_dependencies: None, // No build dependencies are needed.
        // Use what the program as set.
        target: program.manifest.target.clone(),
        features: None, // We don't need features.
        // The package contains one binary.
        bin: vec![Product {
            // We put the source file right next to the Cargo.toml.
            path: Some("main.rs".to_owned()),
            // Use the program name.
            name: Some(program.name.clone()),
            test: false,                   // Not a test.
            doctest: false,                // Not a doctest.
            bench: false,                  // Not a benchmark.
            doc: false,                    // Not a doc.
            plugin: false,                 // Not a plugin.
            proc_macro: false, // Not a proc macro. This may be allowed in the future.
            harness: false,    // No test harness is needed.
            edition: None,     // Use the package edition.
            required_features: Vec::new(), // We don't have any features.
            crate_type: None,  // The default crate type is fine.
        }],
        bench: Vec::new(),   // No benchmarks.
        test: Vec::new(),    // No tests.
        example: Vec::new(), // No examples.
        // Use the patches on the program.
        patch: program.manifest.patch.clone(),
        lib: None, // We don't need a library.
        // Use the profiles on the program.
        profile: program.manifest.profile.clone(),
        badges: None, // We don't need badges.
    }
}
