use std::{
    fmt, fs,
    io::{self, BufReader, ErrorKind, Read},
    process::{Command, Stdio},
};

use cargo_metadata::{
    diagnostic::{Diagnostic, DiagnosticLevel},
    BuildFinished, CompilerMessage, Message,
};
use regex::RegexBuilder;

use crate::{manifest::make_manifest, Binary, Program};

#[derive(thiserror::Error)]
pub enum CompileError {
    #[error("failed create cargo package: {0}")]
    CreatingCargoPackage(io::Error),

    #[error("failed to run `cargo build`: {0}")]
    CargoBuildCommand(io::Error),

    #[error("failed to build test executable because of external error: {0}")]
    ExternalBuild(ExternalBuildErrorReport),

    #[error("build failed:\n{0:#}")]
    Build(BuildErrorReport),
}

impl fmt::Debug for CompileError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use CompileError::*;

        let mut temp = f.debug_struct("CompileError");
        temp.field("message", &self.to_string());

        match self {
            CreatingCargoPackage(err) => {
                temp.field("data", &err);
            }
            CargoBuildCommand(err) => {
                temp.field("data", &err);
            }
            ExternalBuild(_) | Build(_) => {}
        }

        temp.finish()
    }
}

impl CompileError {
    pub fn should_fail(self) -> Result<BuildErrorReport, CompileFailError> {
        match self {
            CompileError::Build(report) => Ok(BuildErrorReport {
                diagnostics: report
                    .diagnostics
                    .into_iter()
                    .filter(|message| {
                        matches!(
                            message.level,
                            DiagnosticLevel::Ice | DiagnosticLevel::Error
                        )
                    })
                    .collect(),
                stderr: report.stderr,
                root_path: report.root_path,
            }),
            err => Err(CompileFailError::CompileError(err)),
        }
    }
}

pub struct ExternalBuildErrorReport {
    pub messages: Vec<CompilerMessage>,
    pub stderr: String,
}

impl fmt::Display for ExternalBuildErrorReport {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for message in &self.messages {
            write!(f, "{}", message)?;
        }

        write!(f, "{}", self.stderr)?;

        Ok(())
    }
}

#[derive(Debug)]
pub struct BuildErrorReport {
    pub diagnostics: Vec<Diagnostic>,
    pub stderr: String,
    pub root_path: String,
}

impl fmt::Display for BuildErrorReport {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if f.alternate() {
            for message in &self.diagnostics {
                // write!(f, "{}", message)?;
                for line in message.to_string().lines() {
                    write!(f, "   ")?;
                    for c in line.chars() {
                        write!(f, "{}", c)?;
                    }
                    writeln!(f)?;
                }
            }

            write!(f, "{}", self.stderr)?;
        } else {
            // should follow http://bixense.com/clicolors/

            for message in &self.diagnostics {
                let regex = RegexBuilder::new(
                    r"error\[.*\]:.*\s.*-->.*(?:\s.*\d*\s.*\|.*)*\s",
                )
                .multi_line(true)
                .build()
                .unwrap();
                let cleaned =
                    strip_ansi_escapes::strip_str(&message.to_string())
                        .replace(&self.root_path, ".");
                if cleaned.starts_with("error:") {
                    continue;
                }

                if let Some(captures) = regex.captures(&cleaned) {
                    let (cleaned, _) = captures.extract::<0>();
                    write!(f, "{}", cleaned)?;
                } else {
                    write!(f, "{}", cleaned)?;
                }
            }

            // write!(f, "{}", self.stderr.replace(&self.root_path, "."))?;
        }

        Ok(())
    }
}

#[derive(thiserror::Error, Debug)]
pub enum CompileFailError {
    #[error(transparent)]
    CompileError(#[from] CompileError),

    #[error("compile did not fail")]
    NoFail(Binary),
}

type Result<T, E = CompileError> = std::result::Result<T, E>;

pub(crate) fn compile(program: &Program) -> Result<Binary> {
    // Crate directory for cargo package.
    fs::create_dir_all(program.path())
        .map_err(CompileError::CreatingCargoPackage)?;

    // Create cargo manifest.
    // cargo won't rebuild if it didn't change.
    fs::write(
        program.path().join("Cargo.toml"),
        toml::to_string(&make_manifest(program)).unwrap(),
    )
    .map_err(CompileError::CreatingCargoPackage)?;

    fs::copy(
        program.environment.package.manifest_dir.join("Cargo.lock"),
        program.path().join("Cargo.lock"),
    )
    .map_err(CompileError::CreatingCargoPackage)?;

    // cargo won't rebuild if it didn't change.
    fs::write(program.path().join("main.rs"), &program.source)
        .map_err(CompileError::CreatingCargoPackage)?;

    // Set cargo command.
    let mut command =
        std::process::Command::new(&program.environment.toolchain.cargo_path);

    command
        .args([
            "rustc",
            // "-vv",
            "--message-format=json-diagnostic-rendered-ansi",
            // "-q",
            "--manifest-path",
        ])
        .arg(program.path().join("Cargo.toml"))
        // .args(["--color", "always"])
        .arg("--bin")
        .arg(&program.name);

    // command.env("CARGO_LOG", "cargo::core::compiler::fingerprint=info");

    // If the target isn't the host target then we need to tell cargo that.
    if program.environment.toolchain.target_triple
        != program.environment.toolchain.host_triple
    {
        command
            .arg("--target")
            .arg(&program.environment.toolchain.target_triple);
    }

    let flags = &program.environment.toolchain.extra_encoded_rustflags;
    command.env("CARGO_ENCODED_RUSTFLAGS", flags);
    dbg!(flags);

    command
        .arg("--target-dir")
        .arg(&program.environment.package.target_directory);

    // Find the rlib file for the parent package.
    // What to do if more than one is found? Is that even possible?
    // let crate_name = program.environment.package.name.replace('-', "_");
    // let mut parent_rlib = None;
    // for file in std::fs::read_dir(
    //     program.environment.package.artifact_directory.join("deps"),
    // )
    // .unwrap()
    // {
    //     let file = file.unwrap();
    //     if file
    //         .file_name()
    //         .to_string_lossy()
    //         .starts_with(&format!("lib{}", crate_name))
    //         && file.file_name().to_string_lossy().ends_with("rlib")
    //     {
    //         parent_rlib = Some(file);
    //     }
    // }
    // let parent_rlib = parent_rlib.unwrap();

    command.arg("--");

    for (name, dep) in &program.environment.test.dependencies {
        command.args(["--extern", &format!("{}={}", name, dep)]);
    }

    // command.args([
    //     "--",
    //     "--extern",
    //     &format!("{}={}", crate_name, parent_rlib.path().to_string_lossy()),
    // ]);

    command.stdout(Stdio::piped()).stderr(Stdio::piped());

    command.env_remove("__LLVM_PROFILE_RT_INIT_ONCE");

    // eprintln!("{}", command_to_string(&command));
    // todo!();

    let mut spawned =
        command.spawn().map_err(CompileError::CargoBuildCommand)?;

    let reader = BufReader::new(
        spawned
            .stdout
            .take()
            .expect("spawned should have stdout buffer"),
    );

    let mut stderr_stream = spawned.stderr.take().unwrap();
    let handle = std::thread::spawn(move || {
        let mut stderr = String::new();
        stderr_stream.read_to_string(&mut stderr).unwrap();
        stderr
    });

    let messages = Message::parse_stream(reader);

    let mut executable_path = None;

    let mut build_error_report = BuildErrorReport {
        diagnostics: Vec::new(),
        stderr: String::new(),
        root_path: program.environment.package.manifest_dir.to_string(),
    };

    let mut external_build_error_report = ExternalBuildErrorReport {
        messages: Vec::new(),
        stderr: String::new(),
    };

    let mut external_fail = false;

    let mut build_finished = false;

    // Record the path to the artifact and any diagnostics.
    for message in messages {
        match message {
            Ok(Message::CompilerArtifact(artifact))
                if artifact.target.name == program.name =>
            {
                if let Some(path) = artifact.executable {
                    executable_path = Some(path);
                }
            }
            Ok(Message::CompilerMessage(msg)) => {
                if msg.target.name == program.name {
                    build_error_report.diagnostics.push(msg.message.clone());
                } else {
                    use DiagnosticLevel::*;
                    match &msg.message.level {
                        Ice | Error => external_fail = true,
                        _ => {}
                    }
                }

                external_build_error_report.messages.push(msg);
            }
            Ok(Message::BuildFinished(BuildFinished {
                success: true, ..
            })) => build_finished = true,
            _ => {}
        }
    }

    let stderr = handle.join().unwrap();

    // println!("{}", stderr);

    if build_finished {
        if let Some(path) = executable_path {
            Ok(Binary {
                _name: program.name.clone(),
                _environment: program.environment.clone(),
                path: path.into_std_path_buf(),
            })
        } else {
            Err(CompileError::CargoBuildCommand(io::Error::new(
                ErrorKind::Other,
                "No executable was generated.",
            )))
        }
    } else if external_fail {
        external_build_error_report.stderr = stderr;
        Err(CompileError::ExternalBuild(external_build_error_report))
    } else {
        build_error_report.stderr = stderr;
        Err(CompileError::Build(build_error_report))
    }
}

pub fn command_to_string(command: &Command) -> String {
    use std::fmt::Write;

    let mut buf = String::new();

    write!(
        &mut buf,
        "{}",
        command
            .get_envs()
            .flat_map(|(name, value)| value.map(|value| (name, value)))
            .map(|(name, value)| format!(
                "{}={}",
                name.to_string_lossy(),
                value.to_string_lossy()
            ))
            .collect::<Vec<_>>()
            .join(" ")
    )
    .unwrap();
    write!(
        &mut buf,
        " {}",
        command
            .get_current_dir()
            .unwrap_or(&std::env::current_dir().unwrap())
            .to_string_lossy()
    )
    .unwrap();
    write!(&mut buf, " {}", command.get_program().to_string_lossy()).unwrap();
    write!(
        &mut buf,
        " {}",
        command
            .get_args()
            .map(|x| x.to_string_lossy().into_owned())
            .collect::<Vec<_>>()
            .join(" ")
    )
    .unwrap();

    buf
}
