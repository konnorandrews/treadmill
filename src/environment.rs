//! Information about the test environment.
//!
//! All treadmill programs are run within either a unit test or integration test.
//! This module provides information about that test environment.

use std::{
    collections::BTreeMap,
    env,
    ffi::OsString,
    fmt,
    fs::File,
    io, path::PathBuf,
};

use cargo_manifest::{Edition, Manifest, MaybeInherited, Resolver};
use cargo_metadata::{camino::Utf8PathBuf, MetadataCommand};
use serde::Deserialize;

/// Possible errors when detecting the test environment.
pub struct DetectEnvironmentError {
    inner: ErrorKind,
}

enum ErrorKind {
    /// Environment variable is missing.
    MissingEnvVar(&'static str),

    /// Failed to get metadata from Cargo.
    CargoMetadata(cargo_metadata::Error),

    /// Current package, as given by Cargo, is not in the workspace.
    MissingPackage { name: String },

    /// Failed to get path to current executable.
    MissingCurrentExe(io::Error),

    /// Attempted to convert a Os string to a normal string.
    NonUtf8(OsString),

    /// Miri was detected.
    MiriDetected,

    /// Path doesn't have the expected parent.
    MissingPathParent(Utf8PathBuf),

    /// Path doesn't have a file name.
    NoFileName(PathBuf),
}

impl ErrorKind {
    fn to(self) -> DetectEnvironmentError {
        DetectEnvironmentError { inner: self }
    }
}

impl std::error::Error for DetectEnvironmentError {}

impl fmt::Debug for DetectEnvironmentError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self)
    }
}

impl fmt::Display for DetectEnvironmentError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match &self.inner {
            ErrorKind::MissingEnvVar(name) => {
                write!(f, "missing expected environment variable '{}'", name)
            }
            ErrorKind::CargoMetadata(err) => {
                write!(f, "unable to query Cargo for metadata: {}", err)
            }
            ErrorKind::MissingPackage { name } => {
                write!(
                    f,
                    "can not find package '{}' in the Cargo workspace",
                    name
                )
            }
            ErrorKind::MissingCurrentExe(err) => {
                write!(f, "can not find current executable: {}", err)
            }
            ErrorKind::NonUtf8(str) => {
                write!(f, "non-utf8 string or path: \"{:?}\"", str)
            }
            ErrorKind::MiriDetected => {
                write!(
                    f,
                    "Miri was detected, treadmill cannot run inside of Miri"
                )
            }
            ErrorKind::MissingPathParent(path) => {
                write!(f, "expected path to have a parent: \"{}\"", path)
            }
            ErrorKind::NoFileName(path) => {
                write!(f, "path has no file name: \"{}\"", path.display())
            }
        }
    }
}

type Result<T, E = DetectEnvironmentError> = std::result::Result<T, E>;

/// Information about the Rust toolchain.
#[non_exhaustive]
#[derive(Debug, PartialEq, Clone)]
pub struct Toolchain {
    /// Path to Cargo executable.
    pub cargo_path: Utf8PathBuf,

    /// Target triple being compiled for.
    pub target_triple: String,

    /// Host triple of Cargo.
    pub host_triple: String,

    /// Extra flags to invoke `rustc` with.
    pub extra_encoded_rustflags: String,

    /// Flag for if the toolchain is a nightly version.
    pub is_nightly: bool,

    /// Path to rustup if detected.
    ///
    /// This allows switching to nightly if requested.
    pub rustup_path: Option<Utf8PathBuf>,
}

/// Information about the package.
#[non_exhaustive]
#[derive(Debug, PartialEq, Clone)]
pub struct Package {
    /// Name of the current package.
    pub name: String,

    /// Directory containing the manifest of the package.
    pub manifest_dir: Utf8PathBuf,

    /// Rust edition for the package.
    pub edition: cargo_manifest::Edition,

    /// Target directory for the package.
    pub target_directory: Utf8PathBuf,

    /// Directory for where artifacts will be placed.
    pub artifact_directory: Utf8PathBuf,

    /// Full manifest for the package.
    pub manifest: Manifest,

    /// Resolver for Cargo to use.
    pub resolver: Resolver,
}

/// Information about test executable.
#[non_exhaustive]
#[derive(Debug, PartialEq, Clone)]
pub struct Test {
    /// Name of the current test executable.
    pub name: String,

    /// List of the exact dependencies the test uses.
    ///
    /// The paths are to the rlib files Cargo has already generated for them.
    pub dependencies: BTreeMap<String, Utf8PathBuf>,
}

/// Information about the environment of the current executable.
#[non_exhaustive]
#[derive(Debug, PartialEq, Clone)]
pub struct Environment {
    /// Toolchain information.
    pub toolchain: Toolchain,

    /// Package information.
    pub package: Package,

    /// Current test information.
    pub test: Test,

    /// Cargo manifest for the workspace.
    pub workspace_manifest: Option<Manifest>,
}

impl Environment {
    /// Detect the current environment.
    ///
    /// This function must be called from a executable being run by Cargo as a test.
    pub fn detect() -> Result<Self> {
        // Do a check for miri as we can't run in it.
        if cfg!(miri) || env::vars_os().count() == 0 {
            // We would expect any normal environment to have some environment variables,
            // but miri doesn't. We can't do anything anyways if this is a normal
            // platform, as we needed those environment variables for detecting Cargo.
            return Err(ErrorKind::MiriDetected.to());
        }

        // Get the manifest folder of the parent package.
        // The parent package being the one treadmill is being used in.
        let package_manifest_dir = env_package_manifest_dir()?;

        // Get metadata about the project from Cargo directly.
        // This will run the `cargo metadata` command.
        //
        // The cargo manifest is always `Cargo.toml`
        // https://doc.rust-lang.org/cargo/reference/manifest.html
        let metadata = MetadataCommand::new()
            .manifest_path(package_manifest_dir.join("Cargo.toml"))
            .exec()
            .map_err(ErrorKind::CargoMetadata)
            .map_err(ErrorKind::to)?;

        // Find the package that this test is for.
        //
        // Cargo tells us the package name, but we want to get all its metadata.
        let package = {
            let package_name = env_package_name()?;

            // It shouldn't really be possible for the package to be missing.
            metadata
                .packages
                .iter()
                .find(|&package| package.name == package_name)
                .ok_or_else(|| {
                    ErrorKind::MissingPackage {
                        name: package_name.clone(),
                    }
                    .to()
                })?
        };

        // We will use the parent of the artifact path as the target directory.
        // We do this because we can't be sure that what cargo metadata reports
        // is what is being used by the current command.
        let artifact_path = env_package_artifact_path();
        let target_directory = artifact_path
            .parent()
            .ok_or_else(|| {
                ErrorKind::MissingPathParent(artifact_path.clone()).to()
            })?
            .to_path_buf();

        // Get the name of the current test executable.
        // Cargo names them `<name>-<hash>`.
        // The hash will give us enough information to reverse engineer the dependency
        // artifacts later.
        let (test_name, test_hash) = {
            // The current binary is the test environment, and cargo names it.
            let current_exe = env::current_exe()
                .map_err(ErrorKind::MissingCurrentExe)
                .map_err(ErrorKind::to)?;

            let name = current_exe
                .file_name()
                .map(|x| x.to_os_string())
                .ok_or_else(|| {
                    ErrorKind::NoFileName(current_exe).to()
                })?
                .to_string_lossy()
                .into_owned();

            if let Some((test_name, test_hash)) = name.rsplit_once('-') {
                (test_name.to_owned(), Some(test_hash.to_owned()))
            } else {
                (name, None)
            }
        };

        // Get the target to build for.
        let target = env_target().to_owned();

        let host = env_host().to_owned();

        let encoded_rustflags = env_encoded_rustflags().to_owned();

        // Get path to cargo so we can use it to compile.
        let cargo_path = env_cargo_path()?;

        let manifest = Manifest::from_path(&package.manifest_path).unwrap();

        let workspace_manifest =
            Manifest::from_path(metadata.workspace_root.join("Cargo.toml"))
                .unwrap();

        let in_workspace = manifest != workspace_manifest;

        let resolver = workspace_manifest
            .workspace
            .as_ref()
            .map(|workspace| workspace.resolver.unwrap_or(Resolver::V1))
            .unwrap_or_else(|| {
                manifest
                    .package
                    .as_ref()
                    .map(|package| {
                        package.resolver.unwrap_or_else(|| {
                            match package.edition.as_ref().unwrap_or(
                                &cargo_manifest::MaybeInherited::Local(
                                    Edition::E2015,
                                ),
                            ) {
                                MaybeInherited::Inherited { .. } => {
                                    panic!("what?")
                                }
                                MaybeInherited::Local(Edition::E2015) => {
                                    Resolver::V1
                                }
                                MaybeInherited::Local(Edition::E2018) => {
                                    Resolver::V1
                                }
                                MaybeInherited::Local(Edition::E2021) => {
                                    Resolver::V2
                                }
                            }
                        })
                    })
                    .unwrap_or(Resolver::V2)
            });

        let artifact_directory = env_package_artifact_path();

        // dbg!(&test_hash);

        // Check if for the lib test file.
        let mut fingerprint_path = artifact_directory
            .join(".fingerprint")
            .join(format!("{}-{}", package.name, test_hash.as_ref().unwrap()))
            .join(format!("test-lib-{}.json", test_name));
        if !fingerprint_path.exists() {
            // Construct path to the integration test fingerprint file.
            fingerprint_path = artifact_directory
                .join(".fingerprint")
                .join(format!("{}-{}", package.name, test_hash.unwrap()))
                .join(format!("test-integration-test-{}.json", test_name));
        }

        let x: CargoFingerprint =
            serde_json::from_reader(File::open(fingerprint_path).unwrap())
                .unwrap();
        // dbg!(x);

        // for dep in x.deps {
        //     let hash = format!("{:x}", dep.hash);
        //     dbg!(hash, dep.name);
        // }

        // For each fingerprint dep we search for the metadata hash.
        // eprintln!("Scanning for direct dependents...");
        let mut deps = BTreeMap::new();
        for dep in x.deps {
            let wanted_hash = format!("{:016x}", dep.hash.swap_bytes());

            if dep.name == "build_script_build" {
                continue;
            }

            let mut meta_hash = None;
            for file in
                std::fs::read_dir(artifact_directory.join(".fingerprint"))
                    .unwrap()
            {
                let file = file.unwrap();

                for file in std::fs::read_dir(file.path()).unwrap() {
                    let file = file.unwrap();
                    if file.file_name().to_string_lossy().replace('_', "-")
                        == format!("lib-{}", dep.name.replace('_', "-"))
                    {
                        // Read the file to get the fingerprint hash.
                        let hash =
                            std::fs::read_to_string(file.path()).unwrap();
                        if wanted_hash == hash {
                            let file_name = file
                                .path()
                                .parent()
                                .unwrap()
                                .file_name()
                                .unwrap()
                                .to_string_lossy()
                                .into_owned();
                            let (_package_name, metadata_hash) =
                                file_name.rsplit_once('-').unwrap();
                            meta_hash = Some(metadata_hash.to_owned());
                        }
                    }
                }
            }
            if meta_hash.is_none() {
                panic!("badness 10000, {}, {}", dep.name, wanted_hash);
            }
            let meta_hash = meta_hash.unwrap();

            // Find the artifact.
            let rlib_path = artifact_directory
                .join("deps")
                .join(format!("lib{}-{}.rlib", dep.name, meta_hash));
            // eprintln!("{}: {}", dep.name, &rlib_path);
            deps.insert(dep.name, rlib_path);
        }

        Ok(Self {
            toolchain: Toolchain {
                cargo_path,
                target_triple: target,
                host_triple: host,
                extra_encoded_rustflags: encoded_rustflags,
                is_nightly: false,
                rustup_path: None,
            },
            package: Package {
                name: package.name.clone(),
                manifest_dir: package_manifest_dir,
                edition: convert_edition(&package.edition),
                manifest,
                resolver,
                target_directory,
                artifact_directory,
            },
            test: Test {
                name: test_name,
                dependencies: deps,
            },
            workspace_manifest: in_workspace.then_some(workspace_manifest),
        })
    }
}

#[derive(Deserialize, Debug)]
struct CargoFingerprint {
    deps: Vec<CargoFingerprintDep>,
}

#[derive(Deserialize, Debug)]
struct CargoFingerprintDep {
    _id: u64,
    name: String,
    _public: bool,
    hash: u64,
}

fn convert_edition(edition: &cargo_metadata::Edition) -> Edition {
    match edition {
        cargo_metadata::Edition::E2015 => Edition::E2015,
        cargo_metadata::Edition::E2018 => Edition::E2018,
        cargo_metadata::Edition::E2021 => Edition::E2021,
        _ => panic!("unknown edition: {}", edition),
    }
}

// -----------------

fn env_target() -> &'static str {
    // This is set by the build script.
    env!("TREADMILL_TARGET")
}

fn env_host() -> &'static str {
    // This is set by the build script.
    env!("TREADMILL_HOST")
}

fn env_encoded_rustflags() -> &'static str {
    // This is set by the build script.
    env!("TREADMILL_ENCODED_FLAGS")
}

// cargo will set these environment variables when using `cargo test`.
// https://doc.rust-lang.org/cargo/reference/environment-variables.html#environment-variables-cargo-sets-for-crates

fn env_package_artifact_path() -> Utf8PathBuf {
    let mut path = Utf8PathBuf::from(env!("TREADMILL_OUT_DIR"));
    // build/<crate>/out
    path.pop();
    path.pop();
    path.pop();
    path
}

fn env_cargo_path() -> Result<Utf8PathBuf> {
    Utf8PathBuf::from_path_buf(
        env::var_os("CARGO")
            .ok_or(ErrorKind::MissingEnvVar("CARGO").to())?
            .into(),
    )
    .map_err(|path| ErrorKind::NonUtf8(path.into_os_string()).to())
}

/// Get the path to the parent package's Cargo manifest directory.
///
/// Cargo will always provide this when running test binaries.
fn env_package_manifest_dir() -> Result<Utf8PathBuf> {
    // This environment variable is set when running a test by Cargo.
    // https://doc.rust-lang.org/cargo/reference/environment-variables.html#environment-variables-cargo-sets-for-crates
    Utf8PathBuf::from_path_buf(
        env::var_os("CARGO_MANIFEST_DIR")
            .ok_or(ErrorKind::MissingEnvVar("CARGO_MANIFEST_DIR").to())?
            .into(),
    )
    .map_err(|path| ErrorKind::NonUtf8(path.into_os_string()).to())
}

/// Get the name of the package.
///
/// Note, this is **not** the crate name.
///
/// Cargo will always provide this when running test binaries.
fn env_package_name() -> Result<String> {
    // This environment variable is set when running a test by Cargo.
    // https://doc.rust-lang.org/cargo/reference/environment-variables.html#environment-variables-cargo-sets-for-crates
    env::var_os("CARGO_PKG_NAME")
        .ok_or(ErrorKind::MissingEnvVar("CARGO_PKG_NAME").to())?
        .into_string()
        .map_err(ErrorKind::NonUtf8)
        .map_err(ErrorKind::to)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn can_detect() {
        let detected = Environment::detect().unwrap();

        #[cfg(all(
            target_arch = "x86_64",
            target_vendor = "unknown",
            target_os = "linux",
            target_env = "gnu"
        ))]
        assert_eq!(
            detected.toolchain.target_triple,
            "x86_64-unknown-linux-gnu"
        );

        assert_eq!(detected.package.name, "treadmill");
        assert!(detected.package.manifest_dir.join("Cargo.toml").exists());
        assert_eq!(detected.test.name, "treadmill");
        assert_eq!(detected.package.edition, Edition::E2021);
        assert!(detected.package.target_directory.exists());
    }
}
