/// Independent
mod compile;
mod deps;
pub mod environment;
pub mod manifest;

use std::{
    borrow::Cow,
    path::PathBuf,
    sync::{Once, OnceLock},
};

use cargo_manifest::{
    Dependency, DependencyDetail, DepsSet, PatchSet, Profiles, TargetDepsSet,
};
use cargo_metadata::camino::Utf8PathBuf;
pub use compile::*;
use deps::{add_dependency, normalize_dependencies};
use environment::*;

/// Rust program that can be compiled and run.
#[derive(Debug)]
pub struct Program {
    /// Name of the program.
    ///
    /// Each program is identified by a name.
    /// This allows tests to contain more than one program.
    name: String,

    /// Source code of the program.
    source: String,

    /// Cargo manifest settings.
    manifest: ProgramManifest,

    /// The environment the program is
    environment: Cow<'static, Environment>,
}

impl Program {
    pub fn path(&self) -> Utf8PathBuf {
        self.environment
            .package
            .target_directory
            .join("treadmill")
            .join(&self.environment.package.name)
            .join(&self.environment.test.name)
            .join(&self.name)
    }
}

/// Changeable parts of a program's Cargo manifest.
#[derive(Debug)]
struct ProgramManifest {
    /// Nightly cargo features.
    ///
    /// Should usually mirror the host package.
    cargo_features: Option<Vec<String>>,

    /// The dependencies to add to the program's Cargo manifest.
    ///
    /// Should usually mirror the host package.
    /// With the addition of the package's library as a dependency.
    dependencies: DepsSet,

    /// Target specific dependencies.
    ///
    /// Should usually mirror the host package.
    /// The dev and build sets are ignored.
    target: Option<TargetDepsSet>,

    /// Any crate patches.
    ///
    /// Should usually mirror the host package.
    patch: Option<PatchSet>,

    /// Compilation profiles.
    ///
    /// Should usually mirror the host package.
    profile: Option<Profiles>,
}

/// Get the environment that programs will be created within.
fn shared_env() -> Result<&'static Environment, DetectEnvironmentError> {
    // Environment everything shares.
    // This allows environment detection to happen once, as its kind of expensive.
    static SHARED_ENV: OnceLock<Environment> = OnceLock::new();

    // Force only one detection to happen.
    static DETECTING: Once = Once::new();

    // If the static has an environment then we just return that.
    if let Some(env) = SHARED_ENV.get() {
        return Ok(env);
    }

    // If the static doesn't have an environment we detect it and store it in the static.
    let mut res = Ok(());
    DETECTING.call_once(|| match Environment::detect() {
        Ok(env) => _ = SHARED_ENV.set(env),
        Err(err) => res = Err(err),
    });

    // At this point the static should contain an environment.
    res.map(|()| SHARED_ENV.get().unwrap())
}

impl Program {
    pub fn new(
        name: String,
        source: String,
    ) -> Result<Self, DetectEnvironmentError> {
        // Get the current environment.
        let environment = shared_env()?;

        // Construct a dependency for the parent package.
        // let parent_dependency = Dependency::Detailed(DependencyDetail {
        //     path: Some(environment.package.manifest_dir.to_string()),
        //     ..Default::default()
        // });

        let cargo_features =
            environment.package.manifest.cargo_features.clone();
        let target = environment.package.manifest.target.clone();
        let patch = environment.package.manifest.patch.clone();
        let profile = environment.package.manifest.profile.clone();

        // Get the parent crate's dependencies.
        let parent_dependencies =
            environment.package.manifest.dependencies.clone();

        // Get the parent crate's dev dependencies.
        let parent_dev_dependencies =
            environment.package.manifest.dev_dependencies.clone();

        let mut dependencies = DepsSet::new();

        // Add parent package's dependencies.
        for (name, dep) in parent_dependencies.into_iter().flatten() {
            add_dependency(&mut dependencies, name, dep, environment);
        }

        // Add the dev dependencies as overrides.
        for (name, dep) in parent_dev_dependencies.into_iter().flatten() {
            add_dependency(&mut dependencies, name, dep, environment);
        }

        // Add dependency on parent package.
        // add_dependency(
        //     &mut dependencies,
        //     environment.package.name.clone(),
        //     parent_dependency,
        //     environment,
        // );

        normalize_dependencies(&mut dependencies, environment);

        Ok(Program {
            name,
            source,
            manifest: ProgramManifest {
                cargo_features,
                dependencies,
                target,
                patch,
                profile,
            },
            environment: Cow::Borrowed(environment),
        })
    }

    pub fn dependency(
        &mut self,
        name: &str,
        dependency: DependencyDetail,
    ) -> &mut Self {
        add_dependency(
            &mut self.manifest.dependencies,
            name.to_owned(),
            Dependency::Detailed(dependency),
            &self.environment,
        );

        normalize_dependencies(
            &mut self.manifest.dependencies,
            &self.environment,
        );

        self
    }

    // pub fn run_with_miri(&self) {
    //     let program = self;
    //
    //     // Crate directory for cargo package.
    //     fs::create_dir_all(program.path())
    //         .map_err(CompileError::CreatingCargoPackage)
    //         .unwrap();
    //
    //     // Create cargo manifest.
    //     // cargo won't rebuild if it didn't change.
    //     fs::write(
    //         program.path().join("Cargo.toml"),
    //         toml::to_string(&make_manifest(program)).unwrap(),
    //     )
    //     .map_err(CompileError::CreatingCargoPackage)
    //     .unwrap();
    //
    //     fs::copy(
    //         program.environment.package.manifest_dir.join("Cargo.lock"),
    //         program.path().join("Cargo.lock"),
    //     )
    //     .map_err(CompileError::CreatingCargoPackage)
    //     .unwrap();
    //
    //     // cargo won't rebuild if it didn't change.
    //     fs::write(program.path().join("main.rs"), &program.source)
    //         .map_err(CompileError::CreatingCargoPackage)
    //         .unwrap();
    //
    //     // Set cargo command.
    //     let mut command = std::process::Command::new(
    //         &program.environment.toolchain.cargo_path,
    //     );
    //
    //     command
    //         .args([
    //             "miri",
    //             "run",
    //             // "-vv",
    //             "--message-format=json-diagnostic-rendered-ansi",
    //             // "-q",
    //             "--manifest-path",
    //         ])
    //         .arg(program.path().join("Cargo.toml"))
    //         // .args(["--color", "always"])
    //         .arg("--bin")
    //         .arg(&program.name);
    //
    //     // command.env("CARGO_LOG", "cargo::core::compiler::fingerprint=info");
    //
    //     // If the target isn't the host target then we need to tell cargo that.
    //     if program.environment.toolchain.target_triple
    //         != program.environment.toolchain.host_triple
    //     {
    //         command
    //             .arg("--target")
    //             .arg(&program.environment.toolchain.target_triple);
    //     }
    //
    //     let flags = &program.environment.toolchain.extra_encoded_rustflags;
    //     command.env("CARGO_ENCODED_RUSTFLAGS", flags);
    //     dbg!(flags);
    //
    //     command
    //         .arg("--target-dir")
    //         .arg(&program.environment.package.target_directory);
    //
    //     // // Find the rlib file for the parent package.
    //     // // What to do if more than one is found? Is that even possible?
    //     // let crate_name = program.environment.package.name.replace('-', "_");
    //     // let mut parent_rlib = None;
    //     // for file in std::fs::read_dir(
    //     //     program.environment.package.artifact_directory.join("deps"),
    //     // )
    //     // .unwrap()
    //     // {
    //     //     let file = file.unwrap();
    //     //     if file
    //     //         .file_name()
    //     //         .to_string_lossy()
    //     //         .starts_with(&format!("lib{}", crate_name))
    //     //         && file.file_name().to_string_lossy().ends_with("rlib")
    //     //     {
    //     //         parent_rlib = Some(file);
    //     //     }
    //     // }
    //     // let parent_rlib = parent_rlib.unwrap();
    //     //
    //     // command.args([
    //     //     "--",
    //     //     "--extern",
    //     //     &format!("{}={}", crate_name, parent_rlib.path().to_string_lossy()),
    //     // ]);
    //
    //     command.stdout(Stdio::piped()).stderr(Stdio::piped());
    //
    //     command.env_remove("__LLVM_PROFILE_RT_INIT_ONCE");
    //
    //     // eprintln!("{}", command_to_string(&command));
    //     // todo!();
    //
    //     let mut spawned = command
    //         .spawn()
    //         .map_err(CompileError::CargoBuildCommand)
    //         .unwrap();
    //
    //     let reader = BufReader::new(
    //         spawned
    //             .stdout
    //             .take()
    //             .expect("spawned should have stdout buffer"),
    //     );
    //
    //     let mut stderr_stream = spawned.stderr.take().unwrap();
    //     let handle = std::thread::spawn(move || {
    //         let mut stderr = String::new();
    //         stderr_stream.read_to_string(&mut stderr).unwrap();
    //         stderr
    //     });
    //
    //     let messages = Message::parse_stream(reader);
    //
    //     let mut executable_path = None;
    //
    //     let mut build_error_report = BuildErrorReport {
    //         diagnostics: Vec::new(),
    //         stderr: String::new(),
    //         root_path: program.environment.package.manifest_dir.to_string(),
    //     };
    //
    //     let mut external_build_error_report = ExternalBuildErrorReport {
    //         messages: Vec::new(),
    //         stderr: String::new(),
    //     };
    //
    //     let mut external_fail = false;
    //
    //     let mut build_finished = false;
    //
    //     // Record the path to the artifact and any diagnostics.
    //     for message in messages {
    //         match message {
    //             Ok(Message::CompilerArtifact(artifact))
    //                 if artifact.target.name == program.name =>
    //             {
    //                 if let Some(path) = artifact.executable {
    //                     executable_path = Some(path);
    //                 }
    //             }
    //             Ok(Message::CompilerMessage(msg)) => {
    //                 if msg.target.name == program.name {
    //                     build_error_report
    //                         .diagnostics
    //                         .push(msg.message.clone());
    //                 } else {
    //                     use DiagnosticLevel::*;
    //                     match &msg.message.level {
    //                         Ice | Error => external_fail = true,
    //                         _ => {}
    //                     }
    //                 }
    //
    //                 external_build_error_report.messages.push(msg);
    //             }
    //             Ok(Message::BuildFinished(BuildFinished {
    //                 success: true,
    //                 ..
    //             })) => build_finished = true,
    //             _ => {}
    //         }
    //     }
    //
    //     let stderr = handle.join().unwrap();
    //
    //     println!("{}", stderr);
    // }

    #[track_caller]
    pub fn compile(&self) -> Binary {
        match compile(self) {
            Ok(binary) => binary,
            Err(err) => {
                panic!("failed to compile program `{}`, {}", self.name, err)
            }
        }
    }

    #[track_caller]
    pub fn compile_should_fail(&self) -> BuildErrorReport {
        match compile(self) {
            Ok(_) => panic!("Compilation of `{}` did not fail.", self.name),
            Err(err) => match err.should_fail() {
                Ok(report) => report,
                Err(err) => panic!(
                    "Compilation of `{}` failed in an unexpected way: {}",
                    self.name, err
                ),
            },
        }
    }

    pub fn try_compile(&self) -> Result<Binary, CompileError> {
        compile(self)
    }
}

#[derive(Debug)]
pub struct Binary {
    _name: String,
    _environment: Cow<'static, Environment>,
    path: PathBuf,
}

impl Binary {
    pub fn command(&self) -> std::process::Command {
        std::process::Command::new(&self.path)

        // command.env_remove("__LLVM_PROFILE_RT_INIT_ONCE");

        // Detect where a LLVM profile file should be put, and
        // add the name of the binary.
        // if let Some(cov) = &self.environment.llvm_cov {
        //     let mut profraw_path = cov.profile_file.clone();
        //     if let Some(file_name) = profraw_path.file_stem() {
        //         let file_name = format!(
        //             "{}_{}.{}",
        //             file_name.to_string_lossy(),
        //             self.name,
        //             profraw_path.extension().unwrap().to_string_lossy(),
        //         );
        //         profraw_path.set_file_name(file_name);
        //
        //         command.env("LLVM_PROFILE_FILE", profraw_path);
        //     }
        //     // command.env("LLVM_PROFILE_FILE", &cov.profile_file);
        // }
    }
}

#[doc(hidden)]
pub fn __format_program_name(name: Option<&str>, module_path: &str) -> String {
    if let Some(name) = name {
        module_path.replace("::", "-") + "-" + name
    } else {
        module_path.replace("::", "-")
    }
}

#[doc(hidden)]
#[track_caller]
pub fn __format_program_source(source: &str) -> String {
    match syn::parse_file(source) {
        Ok(file) => prettyplease::unparse(&file),
        Err(err) => panic!("Unable to parse program source: {}", err),
    }
}

#[macro_export]
macro_rules! program {
    ($($name:ident,)? {
        #![treadmill(disable_inline_check)]
        $($code:tt)*
    }) => {{
        match $crate::try_program!($($name,)? {
            #![treadmill(disable_inline_check)]
            $($code)*
        }) {
            Ok(binary) => binary,
            Err(err) => panic!("failed to create program: {}", err),
        }
    }};
    ($($name:ident,)? {$($code:tt)*}) => {{
        match $crate::try_program!($($name,)? {$($code)*}) {
            Ok(binary) => binary,
            Err(err) => panic!("failed to create program: {}", err),
        }
    }};
    ({$($extra:tt)*} $($name:ident,)? {$($code:tt)*}) => {{
        match $crate::try_program!($($name,)? {$($code)* $($extra)*}) {
            Ok(binary) => binary,
            Err(err) => panic!("failed to create program: {}", err),
        }
    }}
}

#[doc(hidden)]
#[macro_export]
macro_rules! current_function {
    () => {{
        fn f() {}
        fn type_name_of<T>(_: T) -> &'static str {
            std::any::type_name::<T>()
        }
        let name = type_name_of(f);
        &name[..name.len() - 3]
    }};
}

#[macro_export]
macro_rules! try_program {
    ($name:ident, {
        #![treadmill(disable_inline_check)]
        $($code:tt)*
    }) => {{
        $crate::Program::new(
            $crate::__format_program_name(::core::option::Option::Some(stringify!($name)), $crate::current_function!()),
            $crate::__format_program_source(stringify!($($code)*))
        )
    }};
    ({
        #![treadmill(disable_inline_check)]
        $($code:tt)*
    }) => {{
        $crate::Program::new(
            $crate::__format_program_name(None, $crate::current_function!()),
            $crate::__format_program_source(stringify!($($code)*))
        )
    }};
    ($($name:ident,)? {$($code:tt)*}) => {{
        #[cfg(not(coverage))]
        mod inline_check {
            $($code)*
        }

        // Make sure there is a main.
        #[cfg(not(coverage))]
        {
            let _ = inline_check::main;
        }

        $crate::try_program!($($name,)? {
            #![treadmill(disable_inline_check)]
            $($code)*
        })
    }}
}

pub fn demo() {
    println!("hi");
}

pub fn demo2() {
    println!("bye");
}

// #[cfg(test)]
// mod test {
//     #[test]
//     fn as_unit_test() {
//         program!(as_unit_test, {
//             pub fn main() {
//                 println!("hello world")
//             }
//         })
//         .compile();
//     }
//
//     #[test]
//     fn as_unit_test_fail() {
//         let compile_error = program!(
//             as_unit_test_fail,
//             {
//                 #![treadmill(disable_inline_check)]
//
//                 pub fn main() {
//                     123
//                 }
//             }
//         )
//         .compile_should_fail();
//
//         insta::assert_display_snapshot!(
//             compile_error,
//             @r###"
//         error[E0308]: mismatched types
//          --> main.rs:2:5
//           |
//         1 | pub fn main() {
//           |              - expected `()` because of default return type
//         2 |     123
//           |     ^^^ expected `()`, found integer
//
//         error: aborting due to 1 previous error
//
//         "###
//         );
//     }
// }
