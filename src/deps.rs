use std::collections::btree_map::Entry;

use cargo_manifest::{
    Dependency, DependencyDetail, DepsSet, InheritedDependencyDetail,
};
use cargo_metadata::camino::{Utf8Path, Utf8PathBuf};

use crate::Environment;

pub fn normalize_dependencies(deps: &mut DepsSet, env: &Environment) {
    for (name, dep) in deps {
        match dep {
            Dependency::Simple(_) => {
                // Simple dependencies are fine as is.
            }
            Dependency::Inherited(inherited) => {
                let mut new_dep =
                    resolve_inherited(name, inherited.clone(), env);

                normalize_detailed_dependency(&mut new_dep, env);

                *dep = Dependency::Detailed(new_dep);
            }
            Dependency::Detailed(detailed) => {
                normalize_detailed_dependency(detailed, env);
            }
        }
    }
}

fn normalize_detailed_dependency(
    dep: &mut DependencyDetail,
    env: &Environment,
) {
    // Remove the optional-ness as we have no features to enable.
    dep.optional = None;

    match (&dep.version, &dep.git, &dep.path) {
        (None, None, None) => {
            // ... what?
        }
        (None, None, Some(path)) => {
            // We just need to fix the path.
            dep.path = Some(fix_path(path, env).into_string());
        }
        (None, Some(_), None) => {
            // We don't need to do anything to normalize git deps.
        }
        (Some(_), None, None) => {
            // We don't need to do anything for version deps.
        }
        (None, Some(_), Some(path)) => {
            // Remove git dep.
            dep.git = None;
            dep.branch = None;
            dep.tag = None;
            dep.rev = None;

            // Fix the path dep.
            dep.path = Some(fix_path(path, env).into_string());
        }
        (Some(_), None, Some(path)) => {
            // Remove version dep.
            dep.version = None;
            dep.registry = None;
            dep.registry_index = None;

            // Remove git dep stuff.
            dep.branch = None;
            dep.tag = None;
            dep.rev = None;

            // Fix the path dep.
            dep.path = Some(fix_path(path, env).into_string());
        }
        (Some(_), Some(_), None) => {
            // Remove version dep.
            dep.version = None;
            dep.registry = None;
            dep.registry_index = None;
        }
        (Some(_), Some(_), Some(path)) => {
            // Remove version dep.
            dep.version = None;
            dep.registry = None;
            dep.registry_index = None;

            // Remove git dep.
            dep.git = None;
            dep.branch = None;
            dep.tag = None;
            dep.rev = None;

            // Fix the path dep.
            dep.path = Some(fix_path(path, env).into_string());
        }
    }
}

fn fix_path(path: &str, env: &Environment) -> Utf8PathBuf {
    let path = Utf8Path::new(path);

    if path.is_absolute() {
        // Path is already good.
        path.canonicalize_utf8().unwrap()
    } else {
        // We need to get the absolute path relative to the package root.
        let path = env.package.manifest_dir.join(path);

        path.canonicalize_utf8().unwrap_or(path)
    }
}

pub fn add_dependency(
    deps: &mut DepsSet,
    name: String,
    dep: Dependency,
    env: &Environment,
) {
    match deps.entry(name) {
        Entry::Vacant(entry) => {
            // Add the dependency as is. We will normalize it after.
            entry.insert(dep);
        }
        Entry::Occupied(mut entry) => {
            // We need to merge the dependency definitions.
            merge_dependency(&entry.key().clone(), entry.get_mut(), dep, env);
        }
    }
}

fn merge_dependency(
    name: &str,
    mut original: &mut Dependency,
    new: Dependency,
    env: &Environment,
) {
    match (&mut original, new) {
        (Dependency::Simple(original), Dependency::Simple(new)) => {
            // Replace the version with the new one.
            *original = new;
        }
        (Dependency::Inherited(inherited), Dependency::Simple(new)) => {
            let mut dep = resolve_inherited(name, inherited.clone(), env);

            // Replace the version with the new one.
            dep.version = Some(new);

            *original = Dependency::Detailed(dep);
        }
        (Dependency::Detailed(original), Dependency::Simple(new)) => {
            // Replace the version with the new one.
            original.version = Some(new);
        }
        (Dependency::Inherited(inherited), Dependency::Inherited(new)) => {
            let mut dep = resolve_inherited(name, inherited.clone(), env);

            // Merge the new features.
            if let Some(features) = &mut dep.features {
                features.extend(new.features.into_iter().flatten());
            } else {
                dep.features = new.features;
            }

            *original = Dependency::Detailed(dep);
        }
        (Dependency::Simple(version), Dependency::Inherited(inherited)) => {
            let mut dep = resolve_inherited(name, inherited.clone(), env);

            // Keep the version if the inherited doesn't have one, which
            // should be impossible.
            if dep.version.is_none() {
                dep.version = Some(version.clone());
            }

            *original = Dependency::Detailed(dep);
        }
        (Dependency::Simple(version), Dependency::Detailed(mut new)) => {
            if new.version.is_none() {
                new.version = Some(version.clone());
            }

            *original = Dependency::Detailed(new);
        }
        (Dependency::Inherited(inherited), Dependency::Detailed(new)) => {
            let mut dep = resolve_inherited(name, inherited.clone(), env);

            merge_detailed_dependency(&mut dep, new);

            *original = Dependency::Detailed(dep);
        }
        (Dependency::Detailed(original), Dependency::Inherited(inherited)) => {
            merge_detailed_dependency(
                original,
                resolve_inherited(name, inherited, env),
            );
        }
        (Dependency::Detailed(original), Dependency::Detailed(new)) => {
            merge_detailed_dependency(original, new);
        }
    }
}

fn resolve_inherited(
    name: &str,
    inherited: InheritedDependencyDetail,
    env: &Environment,
) -> DependencyDetail {
    // Find the dependency by name.
    let (_, dep) = env
        .workspace_manifest
        .as_ref()
        .unwrap()
        .workspace
        .as_ref()
        .unwrap()
        .dependencies
        .as_ref()
        .unwrap()
        .iter()
        .find(|(dep_name, _)| dep_name == &name)
        .unwrap();

    match dep {
        Dependency::Simple(version) => DependencyDetail {
            version: Some(version.clone()),
            // Copy the features from the inherited entry.
            features: inherited.features,
            ..Default::default()
        },
        Dependency::Inherited(_) => {
            panic!("how?");
        }
        Dependency::Detailed(dep) => {
            let mut dep = dep.clone();

            // Copy the features from the inherited entry.
            if let Some(features) = &mut dep.features {
                features.extend(inherited.features.into_iter().flatten());
            } else {
                dep.features = inherited.features;
            }

            dep
        }
    }
}

fn merge_detailed_dependency(
    original: &mut DependencyDetail,
    new: DependencyDetail,
) {
    if new.version.is_some() {
        original.version = new.version;
    }

    if new.registry.is_some() {
        original.registry = new.registry;
    }

    if new.registry_index.is_some() {
        original.registry_index = new.registry_index;
    }

    if new.path.is_some() {
        original.path = new.path;
    }

    if new.git.is_some() {
        original.git = new.git;
    }

    if new.branch.is_some() {
        original.branch = new.branch;
    }

    if new.tag.is_some() {
        original.tag = new.tag;
    }

    if new.rev.is_some() {
        original.rev = new.rev;
    }

    // Merge the features instead of replacing them.
    if let Some(features) = &mut original.features {
        features.extend(new.features.into_iter().flatten());
    } else {
        original.features = new.features;
    }

    if new.optional.is_some() {
        original.optional = new.optional;
    }

    if new.default_features.is_some() {
        original.default_features = new.default_features;
    }

    if new.package.is_some() {
        original.package = new.package;
    }
}
